package com.example.makconfig

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Resources
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileReader


class MainActivity : AppCompatActivity() {
    private val CHANGE_PIN_ACTIVITY = 1

    private val configFilename = "config.json"

    private var activityContext: Context? = null
    private var config = Config(
        enableUnlock = false, enableLock = false, enableMobileDataUsage = false,
        screenLockPINHash = null, algorithmSensitivity = "High", useGesture = false
    )

    private class GetConfigStateBroadcastReceiver(val mainActivity: MainActivity) : BroadcastReceiver() {
        override fun onReceive(context: Context?, i: Intent?) {
            Intent().also { intent ->
                intent.action = "CHANGE_CONFIG"
                intent.putExtra("screenLockPINHash", mainActivity.config.screenLockPINHash)
                intent.putExtra("algorithmSensitivity", mainActivity.config.algorithmSensitivity)
                intent.putExtra("enableLock", mainActivity.config.enableLock)
                intent.putExtra("enableMobileDataUsage", mainActivity.config.enableMobileDataUsage)
                intent.putExtra("enableUnlock", mainActivity.config.enableUnlock)
                intent.putExtra("useGesture", mainActivity.config.useGesture)
                mainActivity.sendBroadcast(intent)
            }
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        activityContext = this.baseContext
        deviceIdLabel.text = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        loadConfig(this.baseContext)

        if (config.enableUnlock)
            unlockSwitch.isChecked = true
        if (config.enableLock)
            lockSwitch.isChecked = true
        if (config.enableMobileDataUsage)
            mobileSwitch.isChecked = true

        val getConfigReceiver = GetConfigStateBroadcastReceiver(this)
        val filter = IntentFilter("GET_CONFIG_STATE")
        registerReceiver(getConfigReceiver, filter)

        unlockSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            enableUnlock(buttonView, isChecked)
        }
        lockSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            enableLock(buttonView, isChecked)
        }
        mobileSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            enableMobileData(buttonView, isChecked)
        }
        gestureSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            useGesture(buttonView, isChecked)
        }

        val spinner: Spinner = findViewById(R.id.sensitivitySpinner)
        ArrayAdapter.createFromResource(
            this,
            R.array.sensitivity_levels_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(getIndex(spinner, config.algorithmSensitivity))
        }

        spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>,
                selectedItemView: View,
                position: Int,
                id: Long
            ) {
                config.algorithmSensitivity = spinner.selectedItem as String
                saveConfig(selectedItemView.context)
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
            }

        }
    }

    private fun getIndex(spinner: Spinner, myString: String): Int {
        for (i in 0 until spinner.count) {
            if (spinner.getItemAtPosition(i).toString().equals(myString, ignoreCase = true)) {
                return i
            }
        }

        throw Resources.NotFoundException()
    }

    private fun enableUnlock(buttonView: CompoundButton, isChecked: Boolean) {
        config.enableUnlock = isChecked
        saveConfig(buttonView.context)
    }

    private fun enableLock(buttonView: CompoundButton, isChecked: Boolean) {
        config.enableLock = isChecked
        saveConfig(buttonView.context)
    }

    private fun enableMobileData(buttonView: CompoundButton, isChecked: Boolean) {
        config.enableMobileDataUsage = isChecked
        saveConfig(buttonView.context)
    }

    private fun useGesture(buttonView: CompoundButton, isChecked: Boolean) {
        config.useGesture = isChecked
        saveConfig(buttonView.context)
    }

    fun setPIN(view: View) {
        val intent = Intent(this, ChangePinActivity::class.java).apply {
            putExtra("pinHash", config.screenLockPINHash)
        }
        startActivityForResult(intent, CHANGE_PIN_ACTIVITY)
    }

    private fun saveConfig(context: Context) {
        val gson = Gson()
        val jsonString = gson.toJson(config)

        val file = File(context.filesDir, configFilename)
        file.writeText(jsonString)

        Intent().also { intent ->
            intent.action = "CHANGE_CONFIG"
            intent.putExtra("screenLockPINHash", config.screenLockPINHash)
            intent.putExtra("algorithmSensitivity", config.algorithmSensitivity)
            intent.putExtra("enableLock", config.enableLock)
            intent.putExtra("enableMobileDataUsage", config.enableMobileDataUsage)
            intent.putExtra("enableUnlock", config.enableUnlock)
            intent.putExtra("useGesture", config.useGesture)
            sendBroadcast(intent)
        }
    }

    private fun loadConfig(context: Context) {
        val gson = Gson()
        val file = File(context.filesDir, configFilename)

        if(file.exists()) {
            val reader = FileReader(file)
            config = gson.fromJson(reader, Config::class.java)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CHANGE_PIN_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                config.screenLockPINHash = data!!.getStringExtra("pinHash") as String
                activityContext?.let { saveConfig(it) }
            }
        }
        activityContext?.let { saveConfig(it) }
    }

}
