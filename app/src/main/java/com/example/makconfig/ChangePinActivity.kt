package com.example.makconfig

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import org.mindrot.jbcrypt.BCrypt

class ChangePinActivity : AppCompatActivity() {
    private var pinHash: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_pin)

        pinHash = intent.getStringExtra("pinHash")
    }

    fun changePin(view: View) {
        val oldPinEditor = findViewById<EditText>(R.id.oldPinEditor)
        val oldPin = oldPinEditor.text.toString()

        val newPinEditor = findViewById<EditText>(R.id.newPinEditor)
        val newPin = newPinEditor.text.toString()

        val confirmPinEditor = findViewById<EditText>(R.id.confirmPinEditor)
        val confirmPin = confirmPinEditor.text.toString()

        if (pinHash != null && !BCrypt.checkpw(oldPin, pinHash)) {
            oldPinEditor.error = "Old password is not correct"
            return
        }

        if (newPin.length != 4) {
            newPinEditor.error = "You must have 4 characters in your PIN"
            return
        }

        if (newPin != confirmPin) {
            confirmPinEditor.error = "Passwords do not match"
            return
        }

        val newPinHash = BCrypt.hashpw(newPin, BCrypt.gensalt())

        val resultIntent = Intent()
        resultIntent.putExtra("pinHash", newPinHash)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }
}